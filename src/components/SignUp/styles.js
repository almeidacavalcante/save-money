import styled from 'styled-components';

export const Container = styled.div`
  width: fit-content;
  padding: 16px 26px;
  border-radius: 16px;
  color: #444;
  font-weight: bold;
  background: #ffd782;
  cursor: pointer;
`;
