import styled from 'styled-components';

export const Container = styled.div`
  background: #FFF;
  border-radius: 25px;
  width: 900px;
  height: 100%;
  margin: 30px auto;
  padding: 10px;
  position: relative;
`;
