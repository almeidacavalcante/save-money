import React from 'react';

import { Container } from './styles';

export default function BaseContainer({children}) {
  return (<Container>
    {children}
  </Container>);
}