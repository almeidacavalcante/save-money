import React from 'react';

import { Container } from './styles';
import SignUp from '../SignUp';
import MenuItem from '../MenuItem';

export default function Header() {
  return (<Container>
    <SignUp />
    <MenuItem text="Sign in"/>
    <MenuItem text="Features" active/>
    <MenuItem text="Pricing"/>
  </Container>);
};