import styled from 'styled-components';

export const Container = styled.div`
  margin: 30px 50px 0;
  position: relative;
  display: flex;
  flex-direction: row-reverse;
`;
