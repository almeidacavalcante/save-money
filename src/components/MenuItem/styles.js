import styled from 'styled-components';

export const Container = styled.div`
  width: fit-content;
  padding: 16px 26px;
  margin: 0 20px 0 0;
  color: ${props => props.active ? props.active : "#999"};
  font-weight: bold;
  cursor: pointer;
`;
