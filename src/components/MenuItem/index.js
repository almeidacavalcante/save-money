import React from 'react';

import { Container } from './styles';

export default function MenuItem({
  text,
  active
}) {
  return (<Container active={active}>
    {text}
  </Container>);
}