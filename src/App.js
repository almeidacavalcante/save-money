import React from 'react';
import './App.css';
import Header from './components/Header';
import BaseContainer from './components/BaseContainer';
import GlobalStyles from './styles/global';

function App() {
  return (
    <>
      <GlobalStyles />
      <BaseContainer>
        <Header />

        {/* ROUTES */}

      </BaseContainer>
    </>
  );
}

export default App;
